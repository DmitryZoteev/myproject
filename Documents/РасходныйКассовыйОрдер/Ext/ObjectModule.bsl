﻿#Область ОбработкаСобытий

Процедура ОбработкаПроведения(Отказ, Режим)                                                

	// регистр Задолжности Приход
	Движения.Задолжности.Записывать = Истина;
	Движение = Движения.Задолжности.Добавить();
	Движение.ВидДвижения = ВидДвиженияНакопления.Приход;
	Движение.Период = Дата;
	Движение.Контрагент = Контрагент;
	Движение.Сумма = Сумма;                 
КонецПроцедуры

#КонецОбласти
