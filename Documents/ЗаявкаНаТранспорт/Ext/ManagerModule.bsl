﻿#Область СлужебныеПроцедурыИФункции

#Область Печать

Процедура Печать(ТабДок, Ссылка) Экспорт   
	Макет = Документы.ЗаявкаНаТранспорт.ПолучитьМакет("Печать");
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ЗаявкаНаТранспорт.ДатаВремяДоставки КАК ДатаВремяДоставки,
	|	ЗаявкаНаТранспорт.ДокументОснование КАК ДокументОснование,
	|	ЗаявкаНаТранспорт.КонтактноеЛицо КАК КонтактноеЛицо,
	|	ЗаявкаНаТранспорт.Номер КАК Номер,
	|	ЗаявкаНаТранспорт.ТелефонКонтактногоЛица КАК ТелефонКонтактногоЛица,
	|	ЗаявкаНаТранспорт.ДокументОснование.Товары.(    
	|		НомерСтроки,
	|		Номенклатура,
	|		Количество,
	|		Цена,
	|		Сумма
	|	) КАК Товары,
	|	ЗаявкаНаТранспорт.КонтактноеЛицо.АдресДоставки КАК АдресДоставки
	|ИЗ
	|	Документ.ЗаявкаНаТранспорт КАК ЗаявкаНаТранспорт
	|ГДЕ
	|	ЗаявкаНаТранспорт.Ссылка В(&Ссылка)";
	Запрос.Параметры.Вставить("Ссылка", Ссылка);
	Выборка = Запрос.Выполнить().Выбрать();

	ОбластьЗаголовок = Макет.ПолучитьОбласть("Заголовок");
	Шапка = Макет.ПолучитьОбласть("Шапка");
	ОбластьТоварыШапка = Макет.ПолучитьОбласть("ТоварыШапка");
	ОбластьТовары = Макет.ПолучитьОбласть("Товары");
	ТабДок.Очистить();

	ВставлятьРазделительСтраниц = Ложь;
	Пока Выборка.Следующий() Цикл
		Если ВставлятьРазделительСтраниц Тогда
			ТабДок.ВывестиГоризонтальныйРазделительСтраниц();
		КонецЕсли;

		ТабДок.Вывести(ОбластьЗаголовок);

		Шапка.Параметры.Заполнить(Выборка);
		ТабДок.Вывести(Шапка, Выборка.Уровень());  
		
		ТабДок.Вывести(ОбластьТоварыШапка);
		ВыборкаТовары = Выборка.Товары.Выбрать();
		Пока ВыборкаТовары.Следующий() Цикл
			ОбластьТовары.Параметры.Заполнить(ВыборкаТовары);
			ТабДок.Вывести(ОбластьТовары, ВыборкаТовары.Уровень());
		КонецЦикла;

		ВставлятьРазделительСтраниц = Истина;
	КонецЦикла;    
КонецПроцедуры            

#КонецОбласти         

#КонецОбласти
